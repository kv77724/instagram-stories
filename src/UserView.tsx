/* eslint-disable */
import React, { memo } from "react";
import { Image, StyleSheet, TouchableOpacity, View } from "react-native";

type Props = {
  onClosePress: () => void;
  profile: string;
  name: string;
  datePublication: string;
};

export default memo(function UserView(props: Props) {
  return (
    <View style={styles.userView}>
      <View style={styles.flex} />
      <TouchableOpacity onPress={props.onClosePress}>
        <Image source={require('./imgs/plus.png')} style={styles.cross} />
      </TouchableOpacity>
    </View>
  );
});

const styles = StyleSheet.create({
  barUsername: {
    flexDirection: "row",
    alignItems: "center",
  },
  image: {
    width: 50,
    height: 50,
    // borderRadius: 25,
    // marginLeft: 8,
  },
  verifyIcon: {
    width: 20,
    height: 20,
    marginLeft: 20,
  },
  userView: {
    flexDirection: "row",
    position: "absolute",
    top: 55,
    width: "98%",
    alignItems: "center",
  },
  name: {
    fontSize: 18,
    fontWeight: "500",
    marginLeft: 12,
    color: "white",
  },
  time: {
    fontSize: 12,
    fontWeight: "400",
    marginTop: 3,
    marginLeft: 12,
    color: "white",
  },
  flex:{ flex: 1 },
  cross: { marginRight: 8, height: 35, width: 35, tintColor: '#ffffff' },
});
