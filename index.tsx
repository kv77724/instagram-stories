import React, { Fragment, useRef, useState } from "react";
import {
	FlatList,
	Modal,
	StyleSheet,
	Text,
	View
} from "react-native";

import { StoryType } from "./src";
import StoryContainer from "./src/StoryContainer";
import UserView from "./src/UserView";

const { CubeNavigationHorizontal } = require("react-native-3dcube-navigation");

type Props = {
  data: StoryType[];
  containerAvatarStyle?: StyleSheet.Styles;
  avatarStyle?: StyleSheet.Styles;
  titleStyle?: StyleSheet.Styles;
  textReadMore?: string;
};

const Stories = (props: Props) => {
  const [isModelOpen, setModel] = useState(false);
  const [currentUserIndex, setCurrentUserIndex] = useState(0);
  const [currentScrollValue, setCurrentScrollValue] = useState(0);
  const modalScroll = useRef(null);
  const {cardComponent: Component,StoryHeaderComponent = UserView,StoryFooterComponent, onStoryCompleted=()=>{}} = props;


  const onStorySelect = (index) => {
    setCurrentUserIndex(index);
    // Fixed 0th card showing issue, when opening clicking on any story banner first time
    setTimeout(() => {
      try {
        modalScroll?.current?.scrollTo(index, false);
      } catch (e) {}
    }, 200);
    setModel(true);
  };

  const onStoryClose = () => {
    setModel(false);
  };

  const onStoryNext = (isScroll: boolean) => {
    const newIndex = currentUserIndex + 1;
	console.log("cakked",isScroll,props.data.length - 1 > currentUserIndex);
	
    if (props.data.length - 1 > currentUserIndex) {
      setCurrentUserIndex(newIndex);
      if (!isScroll) {
        //erro aqui
        try {
			console.log("Ended 1")
			onStoryCompleted(currentUserIndex)
          modalScroll?.current?.scrollTo(newIndex, true);
        } catch (e) {
          console.warn("error=>", e);
        }
      }
    } else {
      setModel(false);
		console.log("Ended last")
		onStoryCompleted(currentUserIndex)

    }
  };

  const onStoryPrevious = (isScroll: boolean) => {
    const newIndex = currentUserIndex - 1;
    if (currentUserIndex > 0) {
      setCurrentUserIndex(newIndex);
      if (!isScroll) {
        modalScroll?.current?.scrollTo(newIndex, true);
      }
    }
  };

  const onScrollChange = (scrollValue) => {
    if (currentScrollValue > scrollValue) {
      onStoryNext(true);
      console.log("next");
      setCurrentScrollValue(scrollValue);
    }
    if (currentScrollValue < scrollValue) {
      onStoryPrevious(false);
      console.log("previous");
      setCurrentScrollValue(scrollValue);
    }
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={props.data}
        horizontal
        showsHorizontalScrollIndicator={false}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => (
			<Component item={item} index={index} data={props.data} onPress={onStorySelect} />
        )}
      />
      <Modal
        animationType="slide"
        transparent={false}
        visible={isModelOpen}
        style={styles.modal}
        onShow={() => {
          if (currentUserIndex > 0) {
			modalScroll?.current?.scrollTo(currentUserIndex, false);
          }
        }}
        onRequestClose={onStoryClose}
      >
        <CubeNavigationHorizontal
          callBackAfterSwipe={(g) => onScrollChange(g)}
          ref={modalScroll}
          style={styles.container}
        >
          {props.data.map((item, index) => (
			<Fragment>
            <StoryContainer
              key={item.title}
              onClose={onStoryClose}
              onStoryNext={onStoryNext}
              onStoryPrevious={onStoryPrevious}
              dataStories={item}
              isNewStory={index !== currentUserIndex}
              textReadMore={props.textReadMore}
			  statusBarHeight={props.statusBarHeight}
			  StoryHeaderComponent={StoryHeaderComponent}
            />
			{StoryFooterComponent ? <StoryFooterComponent {...item} />: null}
			</Fragment>
          ))}
        </CubeNavigationHorizontal>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingBottom: 5,
  },
  modal: {
    flex: 1,
  },
  title: {
    fontSize: 8,
    textAlign: "center",
  },
});

export default Stories;